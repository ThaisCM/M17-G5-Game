﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelector : MonoBehaviour
{
    public static void LoadScene(int level)
    {
        SceneManager.LoadScene(level);
       
    }

    public void LoadSceneFromEarly(int level)
    {
        SceneManager.LoadScene(level);
        GameManager._instance.setGameState(GameStates.playing);
    }
}
